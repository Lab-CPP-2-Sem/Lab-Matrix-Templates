#pragma once
#include "BaseVector.h"

template <typename ItemType, int Size>
class SimpleVector :
	public BaseVector<ItemType, Size>
{
private:
	ItemType buf[Size];
public:
	SimpleVector(const ItemType& defaultElement = 0);
	SimpleVector(const ItemType* ItemTypeArray);
	SimpleVector(const BaseVector<ItemType, Size>& exist);
	SimpleVector(const SimpleVector<ItemType, Size>& exist);
	
	SimpleVector<ItemType, Size>& operator= (const SimpleVector<ItemType, Size>& B);
	
	SimpleVector<ItemType, Size> operator+ (const BaseVector<ItemType, Size>& B) const;
	SimpleVector<ItemType, Size> operator- (const BaseVector<ItemType, Size>& B) const;
	SimpleVector<ItemType, Size> operator- ();

	virtual ItemType getElement(const int index) const;
	virtual void setElement(const int index, const ItemType& value);
	virtual ~SimpleVector();
};

template <typename ItemType, int Size>
SimpleVector<ItemType, Size>::SimpleVector(const ItemType& defaultElement = 0)
{
	for (int i = 0; i < Size; i++)
		buf[i] = defaultElement;
}

template <typename ItemType, int Size>
SimpleVector<ItemType, Size>::SimpleVector(const ItemType* ItemTypeArray)
{
	for (int i = 0; i < Size; i++)
		buf[i] = ItemTypeArray[i];
}

template <typename ItemType, int Size>
SimpleVector<ItemType, Size>::SimpleVector(const BaseVector<ItemType, Size>& exist)
{
	for (int i = 0; i < Size; i++)
		buf[i] = exist.getElement(i);
}

template <typename ItemType, int Size>
SimpleVector<ItemType, Size>::SimpleVector(const SimpleVector<ItemType, Size>& exist)
{
	/*
	Вообще говоря, конструктор копирования в данном случае является излишним.
	Конструктор копирования по умолчанию копирует объект байт-в-байт.
	Поскольку у нас имеется статический массив, то все его элементы будут скопированы.
	Но ситуация изменится если:
	1) мы захотим использовать вместо статического массива динамический
	2) мы будем использовать такой ItemType, который использует динамическое распределение памяти
	*/

	for (int i = 0; i < Size; i++)
		buf[i] = exist.buf[i];
}

template <typename ItemType, int Size>
SimpleVector<ItemType, Size>& SimpleVector<ItemType, Size>::operator= (const SimpleVector<ItemType, Size>& B)
{
	/*смотри комментарий к конструктору копирования*/

	for (int i = 0; i < Size; i++)
		buf[i] = B.buf[i];
	return *this;
}

template <typename ItemType, int Size>
SimpleVector<ItemType, Size> SimpleVector<ItemType, Size>::operator+ (const BaseVector<ItemType, Size>& B) const
{
	SimpleVector<ItemType, Size> toReturn;
	for (int i = 0; i < Size; i++)
		toReturn.buf[i] = buf[i] + B.getElement(i);
	return toReturn;
}

template <typename ItemType, int Size>
SimpleVector<ItemType, Size> SimpleVector<ItemType, Size>::operator- (const BaseVector<ItemType, Size>& B) const
{
	SimpleVector<ItemType, Size> toReturn;
	for (int i = 0; i < Size; i++)
		toReturn.buf[i] = buf[i] - B.getElement(i);
	return toReturn;
}

template <typename ItemType, int Size>
SimpleVector<ItemType, Size> SimpleVector<ItemType, Size>::operator- ()
{
	SimpleVector<ItemType, Size> toReturn;
	for (int i = 0; i < Size; i++)
		toReturn.buf[i] = -buf[i];
	return toReturn;
}

template <typename ItemType, int Size>
ItemType SimpleVector<ItemType, Size>::getElement(const int index) const
{
	//пока без проверки на выход за пределы
	return buf[index];
}

template <typename ItemType, int Size>
void SimpleVector<ItemType, Size>::setElement(const int index, const ItemType& value)
{
	//пока без проверки на выход за пределы
	buf[index] = value;
}

template <typename ItemType, int Size>
SimpleVector<ItemType, Size>::~SimpleVector() {}