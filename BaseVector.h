#pragma once
#include <iostream>

template <typename ItemType, int Size>
class BaseVector
{
private:

public:
	BaseVector() {};
	int size() const { return Size; }
	ItemType* toArray() const;

	virtual BaseVector<ItemType, Size>& operator= (const BaseVector<ItemType, Size>& exist);
	virtual BaseVector<ItemType, Size>& operator+= (const BaseVector<ItemType, Size>& B);
	virtual BaseVector<ItemType, Size>& operator-= (const BaseVector<ItemType, Size>& B);
	virtual bool operator== (const BaseVector<ItemType, Size>& B) const;
	virtual bool operator!= (const BaseVector<ItemType, Size>& B) const;
	virtual ItemType operator* (const BaseVector<ItemType, Size>& B) const;

	virtual ItemType getElement(const int index) const = 0;
	virtual void setElement(const int index, const ItemType& value) = 0;
	virtual ~BaseVector() {}

	template <typename ItemType, int Size>
	friend std::ostream& operator<< (std::ostream& os, const BaseVector<ItemType, Size>& vector);
};

template<typename ItemType, int Size>
ItemType* BaseVector<ItemType, Size>::toArray() const
{
	ItemType* toReturn = new ItemType[Size];
	for (int i = 0; i < Size; i++)
		toReturn[i] = getElement(i);
	return toReturn;
}

template <typename ItemType, int Size>
BaseVector<ItemType, Size>& BaseVector<ItemType, Size>::operator= (const BaseVector<ItemType, Size>& exist)
{
	for (int i = 0; i < Size; i++)
		setElement(i, exist.getElement(i));
	return *this;
}

template <typename ItemType, int Size>
BaseVector<ItemType, Size>& BaseVector<ItemType, Size>::operator+= (const BaseVector<ItemType, Size>& B)
{
	for (int i = 0; i < Size; i++)
		setElement(i, getElement(i) + B.getElement(i));
	return *this;
}

template <typename ItemType, int Size>
BaseVector<ItemType, Size>& BaseVector<ItemType, Size>::operator-= (const BaseVector<ItemType, Size>& B)
{
	for (int i = 0; i < Size; i++)
		setElement(i, getElement(i) - B.getElement(i));
	return *this;
}

template <typename ItemType, int Size>
bool BaseVector<ItemType, Size>::operator== (const BaseVector<ItemType, Size>& B) const
{
	for (int i = 0; i < Size; i++)
		if (getElement(i) != B.getElement(i))
			return false;
	return true;
}

template <typename ItemType, int Size>
bool BaseVector<ItemType, Size>::operator!= (const BaseVector<ItemType, Size>& B) const
{
	for (int i = 0; i < Size; i++)
		if (getElement(i) != B.getElement(i))
			return true;
	return false;
}

template <typename ItemType, int Size>
ItemType BaseVector<ItemType, Size>::operator* (const BaseVector<ItemType, Size>& B) const
{
	ItemType toReturn = (ItemType) 0;

	for (int i = 0; i < Size; i++)
		toReturn += (ItemType) (getElement(i) * B.getElement(i));

	return toReturn;
}

template <typename ItemType, int Size>
std::ostream& operator<< (std::ostream& os, const BaseVector<ItemType, Size>& vector)
{
	for (int i = 0; i < Size; i++)
		os << vector.getElement(i) << "  ";
	os << std::endl;
	return os;
}
