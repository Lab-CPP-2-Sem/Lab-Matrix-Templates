#pragma once
#include <iostream>

template <typename ItemType, int Size>
class BaseMatrix
{
private:
	
public:
	BaseMatrix() {};
	int size() const { return Size; }
	
	virtual ItemType getElement(const int rowIndex, const int columnIndex) const = 0;
	virtual void setElement(const int rowIndex, const int columnIndex, const ItemType& value) = 0;
	
	virtual bool operator== (const BaseMatrix<ItemType,Size>& B) const;
	virtual bool operator!= (const BaseMatrix<ItemType, Size>& B) const;
	virtual BaseMatrix<ItemType, Size>& operator= (const BaseMatrix<ItemType, Size>& exist);
	virtual BaseMatrix<ItemType, Size>& operator+= (const BaseMatrix<ItemType, Size>& B);
	virtual BaseMatrix<ItemType, Size>& operator-= (const BaseMatrix<ItemType, Size>& B);
	virtual void transpose();

	virtual ~BaseMatrix() {}

	template <typename ItemType, int Size>
	friend std::ostream& operator<< (std::ostream& os, const BaseMatrix<ItemType, Size>& matrix);
};

template<typename ItemType, int Size>
bool BaseMatrix<ItemType, Size>::operator==(const BaseMatrix<ItemType, Size>& B) const
{
	int i, j;

	for (i = 0; i < Size; i++)
		for (j = 0; j < Size; j++)
			if (getElement(i, j) != B.getElement(i, j))
				return false;
	
	return true;
}

template<typename ItemType, int Size>
bool BaseMatrix<ItemType, Size>::operator!=(const BaseMatrix<ItemType, Size>& B) const
{
	int i, j;

	for (i = 0; i < Size; i++)
		for (j = 0; j < Size; j++)
			if (getElement(i, j) != B.getElement(i, j))
				return true;

	return false;
}

template<typename ItemType, int Size>
BaseMatrix<ItemType, Size>& BaseMatrix<ItemType, Size>::operator=(const BaseMatrix<ItemType, Size>& exist)
{
	int i, j;

	for (i = 0; i < Size; i++)
		for (j = 0; j < Size; j++)
			setElement(i,j, exist.getElement(i,j));

	return *this;
}

template<typename ItemType, int Size>
BaseMatrix<ItemType, Size>& BaseMatrix<ItemType, Size>::operator+=(const BaseMatrix<ItemType, Size>& B)
{
	int i, j;

	for (i = 0; i < Size; i++)
		for (j = 0; j < Size; j++)
			setElement(i, j, getElement(i, j) + B.getElement(i, j));

	return *this;
}

template<typename ItemType, int Size>
BaseMatrix<ItemType, Size>& BaseMatrix<ItemType, Size>::operator-=(const BaseMatrix<ItemType, Size>& B)
{
	int i, j;

	for (i = 0; i < Size; i++)
		for (j = 0; j < Size; j++)
			setElement(i, j, getElement(i, j) - B.getElement(i, j));

	return *this;
}

template<typename ItemType, int Size>
void BaseMatrix<ItemType, Size>::transpose()
{
	int i, j;
	ItemType temp;

	for (i = 0; i < Size; i++)
		for (j = i + 1; j < Size; j++)
		{
			temp = getElement(i, j);
			setElement(i, j, getElement(j, i));
			setElement(j, i, temp);
		}
}

template <typename ItemType, int Size>
std::ostream& operator<< (std::ostream& os, const BaseMatrix<ItemType, Size>& matrix)
{
	int i, j;
	for (i = 0; i < Size; i++)
	{
		for (j = 0; j < Size; j++)
			os << matrix.getElement(i, j) << "  ";
		os << std::endl;
	}
	os << std::endl;
	return os;
}

