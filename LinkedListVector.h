#pragma once
#include "BaseVector.h"

template <typename ItemType, int Size>
class LinkedListVector :
	public BaseVector <ItemType, Size>
{
private:
	typedef struct LinkedListNode
	{
		ItemType data;
		int index;
		LinkedListNode* next;
	} LinkedListNode;
	
	LinkedListNode* head;
	
public:
	LinkedListVector(int foo = 0);	//вводим фиктивный параметр только для совместимости сравнения с (ItemType) 0
	LinkedListVector(const ItemType* ItemTypeArray);
	LinkedListVector(const BaseVector<ItemType, Size>& exist);
	LinkedListVector(const LinkedListVector<ItemType, Size>& exist);

	virtual LinkedListVector<ItemType, Size>& operator= (const BaseVector<ItemType, Size>& B);
	LinkedListVector<ItemType, Size>& operator= (const LinkedListVector<ItemType, Size>& B);

	LinkedListVector<ItemType, Size> operator+ (const BaseVector<ItemType, Size>& B) const;
	LinkedListVector<ItemType, Size> operator- (const BaseVector<ItemType, Size>& B) const;
	LinkedListVector<ItemType, Size> operator- ();

	virtual ItemType getElement(const int index) const;
	virtual void setElement(const int index, const ItemType& value);

	virtual ~LinkedListVector();
};

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size>::LinkedListVector(int foo)
{
	head = nullptr;
}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size>::LinkedListVector(const ItemType* ItemTypeArray)
{
	for (int i = 0; i < Size; i++)
		if (ItemTypeArray[i] != (ItemType)0)
			setElement(i, ItemTypeArray[i]);
}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size>::LinkedListVector(const BaseVector<ItemType, Size>& exist)
{
	ItemType temp;
	for (int i = 0; i < Size; i++)
	{
		temp = exist.getElement(i);
		if (temp != (ItemType)0)
			setElement(i, temp);
	}
}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size>::LinkedListVector(const LinkedListVector<ItemType, Size>& exist)
{
	LinkedListNode* current = exist.head;

	head = nullptr;
	while (current)
	{
		setElement(current->index, current->data);
		current = current->next;
	}

}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size>& LinkedListVector<ItemType, Size>::operator=(const BaseVector<ItemType, Size>& B)
{
	LinkedListNode* current = head;
	LinkedListNode* temp;
	ItemType value;

	while (current)
	{
		temp = current;
		current = current->next;
		delete temp;
	}
	head = nullptr;

	for (int i = 0; i < Size; i++)
	{
		value = B.getElement(i);
		if (value != (ItemType)0)
			setElement(i, value);
	}

	return *this;
}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size>& LinkedListVector<ItemType, Size>::operator=(const LinkedListVector<ItemType, Size>& B)
{
	LinkedListNode* current = head;
	LinkedListNode* temp;
	
	while (current)
	{
		temp = current;
		current = current->next;
		delete temp;
	}
	head = nullptr;
			
	current = B.head;
	while (current)
	{
		setElement(current->index, current->data);
		current = current->next;
	}

	return *this;
}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size> LinkedListVector<ItemType, Size>::operator+(const BaseVector<ItemType, Size>& B) const
{
	LinkedListVector<ItemType, Size> toReturn(*this);
	return (toReturn+=B);
}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size> LinkedListVector<ItemType, Size>::operator-(const BaseVector<ItemType, Size>& B) const
{
	LinkedListVector<ItemType, Size> toReturn(*this);
	return (toReturn -= B);
}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size> LinkedListVector<ItemType, Size>::operator-()
{
	LinkedListVector<ItemType, Size> toReturn(*this);

	LinkedListNode* current = toReturn.head;

	while (current)
	{
		current->data = -current->data;
		current = current->next;
	}

	return toReturn;
}

template<typename ItemType, int Size>
ItemType LinkedListVector<ItemType, Size>::getElement(const int index) const
{
	LinkedListNode* current = head;
	
	while (current && (index > current->index))
		current = current->next;
	
	if (current && (current->index == index))
		return current->data;
	else
		return (ItemType) 0;
}

template<typename ItemType, int Size>
void LinkedListVector<ItemType, Size>::setElement(const int index, const ItemType& value)
{
	LinkedListNode* current;
	LinkedListNode* previous;
	LinkedListNode* temp;

	if (value != (ItemType)0)	//вставляем или удаляем элемент?
	{
		//вставляем элемент
		
		if (!head)	//список пуст?
		{
			head = new LinkedListNode;
			head->index = index;
			head->data = value;
			head->next = nullptr;
		}
		else
		{
			if (index < head->index)
			{
				//вставляем в начало
				temp = head;
				head = new LinkedListNode;
				head->index = index;
				head->data = value;
				head->next = temp;
			}
			else
			{
				//ищем по списку
				previous = head;
				current = head;

				while (current && (index > current->index))
				{
					previous = current;
					current = current->next;
				}

				if (current && (index == current->index))
				{
					//обновляем существующий элемент
					current->data = value;
				}
				else
				{
					//добавляем новый элемент пocле предыдущего
					temp = new LinkedListNode;
					temp->index = index;
					temp->data = value;
					temp->next = current;
					previous->next = temp;
				}
			}
		}
	}
	else
	{
		//удаляем элемент, если он есть
		if (head)	//искать можно только в непустом списке
		{
			if (head->index == index)
			{
				//отдельно обрабатываем головной элемент
				temp = head;
				head = head->next;
				delete temp;
			}
			else
			{
				//ищем по списку
				previous = head;
				current = head;

				while (current && (index > current->index))
				{
					previous = current;
					current = current->next;
				}

				if (current && (index == current->index))
				{
					//удаляем найденный элемент элемент
					previous->next = current->next;
					delete current;
				}
			}
		}
	}
}

template<typename ItemType, int Size>
LinkedListVector<ItemType, Size>::~LinkedListVector()
{
	LinkedListNode* current = head;
	LinkedListNode* temp;

	while (current)
	{
		temp = current;
		current = current->next;
		delete temp;
	}
}

