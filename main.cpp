// Matrix.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include "SimpleVector.h"
#include "SparcedVector.h"
#include "LinkedListVector.h"
#include "Matrix.h"

int main()
{

	LinkedListVector<int, 4> test;
	std::cout << test;
	test.setElement(0, 1);
	std::cout << test;
	/*test.setElement(2, 2);
	std::cout << test;
	test.setElement(1, 0);
	std::cout << test;
	test.setElement(2, 0);
	std::cout << test;*/
	
	SimpleVector<int, 4> test2;
	test2 = test;

	//std::cout << (test2*test);
	

	Matrix<int, 4, LinkedListVector, LinkedListVector> matrix;
		
	matrix.setElement(0, 0 ,1);
	matrix.setElement(1, 1, 2);
	matrix.setElement(2, 2, 3);
	matrix.setElement(3, 3, 4);
	std::cout << matrix << std::endl;

	Matrix<int, 4, LinkedListVector, SparcedVector> matrix2;
	matrix2 = matrix;
	std::cout << matrix2 << std::endl;
	std::cout << (matrix*matrix2);

	std::cout << (matrix*test);
	
	std::cin.get();
    return 0;
}

