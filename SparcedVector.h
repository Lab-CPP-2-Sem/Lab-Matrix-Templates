#pragma once
#include "BaseVector.h"

template <typename ItemType, int Size>
class SparcedVector :
	public BaseVector <ItemType, Size>
{
private:
	int m_buf_size;
	int m_count;
	ItemType* values;
	int* indexes;
	void extend();
	void reduce();
public:
	SparcedVector(const int buffer_size = 1);
	SparcedVector(const ItemType* ItemTypeArray);
	SparcedVector(const BaseVector<ItemType, Size>& exist);
	SparcedVector(const SparcedVector<ItemType, Size>& exist);
	
	virtual SparcedVector<ItemType, Size>& operator= (const BaseVector<ItemType, Size>& B);
	SparcedVector<ItemType, Size>& operator= (const SparcedVector<ItemType, Size>& B);
	
	SparcedVector<ItemType, Size> operator+ (const BaseVector<ItemType, Size>& B) const;
	SparcedVector<ItemType, Size> operator- (const BaseVector<ItemType, Size>& B) const;
	SparcedVector<ItemType, Size> operator- ();
	
	virtual ItemType getElement(const int index) const;
	virtual void setElement(const int index, const ItemType& value);
	
	virtual ~SparcedVector();
};

template <typename ItemType, int Size>
SparcedVector<ItemType, Size>::SparcedVector(const int buffer_size)
{
	m_buf_size = (buffer_size > 0 ? buffer_size : 1);
	m_count = 0;
	values = new ItemType[m_buf_size];
	indexes = new int[m_buf_size];
}

template<typename ItemType, int Size>
SparcedVector<ItemType, Size>::SparcedVector(const ItemType* ItemTypeArray)
{
	int i;
	
	m_count = 0;
	for (i = 0; i < Size; i++)
		if (ItemTypeArray[i] != (ItemType) 0)
			m_count++;

	m_buf_size = (m_count > 0 ? m_count : 1);
	values = new ItemType[m_buf_size];
	indexes = new int[m_buf_size];
	
	m_count = 0;
	for (i = 0; (i < Size) && (m_count < m_buf_size); i++)
		if (ItemTypeArray[i] != (ItemType) 0)
		{
			values[m_count] = ItemTypeArray[i];
			indexes[m_count] = i;
			m_count++;
		}
}

template<typename ItemType, int Size>
SparcedVector<ItemType, Size>::SparcedVector(const BaseVector<ItemType, Size>& exist)
{
	int i;

	m_count = 0;
	for (i = 0; i < Size; i++)
		if (exist.getElement(i) != (ItemType) 0)
			m_count++;

	m_buf_size = (m_count > 0 ? m_count : 1);
	values = new ItemType[m_buf_size];
	indexes = new int[m_buf_size];

	m_count = 0;
	for (i = 0; (i < Size) && (m_count < m_buf_size); i++)
		if (exist.getElement(i) != (ItemType) 0)
		{
			values[m_count] = exist.getElement(i);
			indexes[m_count] = i;
			m_count++;
		}
}

template<typename ItemType, int Size>
SparcedVector<ItemType, Size>::SparcedVector(const SparcedVector<ItemType, Size>& exist)
{
	/*
	Здесь и далее - замечания по поводу memcpy.
	Массив индексов можно копировать при помощи memcpy.
	Но массив значений следует копировать поэлементно,
	поскольку в общем случае ItemType может представлять собой тип класс,
	в котором используется динамическое распределение памяти
	*/

	m_count = exist.m_count;
	m_buf_size = (m_count > 0 ? m_count : 1);
	
	values = new ItemType[m_buf_size];
	indexes = new int[m_buf_size];

	for (int i = 0; i < m_count; i++)
		values[i] = exist.values[i];
	
	memcpy(indexes, exist.indexes, m_count * sizeof(int));
}

template<typename ItemType, int Size>
SparcedVector<ItemType, Size>& SparcedVector<ItemType, Size>::operator= (const BaseVector<ItemType, Size>& B)
{
	int i;
	ItemType value;

	m_count = 0;
	for (i = 0; i < Size; i++)
		if (B.getElement(i) != (ItemType)0)
			m_count++;

	if ((m_count > m_buf_size) || (2*m_count < m_buf_size))
	{
		m_buf_size = (m_count > 0 ? m_count : 1);
		delete[] values;
		delete[] indexes;
		values = new ItemType[m_buf_size];
		indexes = new int[m_buf_size];
	}
	
	m_count = 0;
	for (i = 0; (i < Size) && (m_count < m_buf_size); i++)
	{
		value = B.getElement(i);
		if (value != (ItemType)0)
		{
			values[m_count] = value;
			indexes[m_count] = i;
			m_count++;
		}
	}
	return *this;
}

template<typename ItemType, int Size>
SparcedVector<ItemType, Size>& SparcedVector<ItemType, Size>::operator= (const SparcedVector<ItemType, Size>& B)
{
	m_count = B.m_count;

	if ((m_count > m_buf_size) || (2 * m_count < m_buf_size))
	{
		m_buf_size = (m_count > 0 ? m_count : 1);
		delete[] values;
		delete[] indexes;
		values = new ItemType[m_buf_size];
		indexes = new int[m_buf_size];
	}

	for (int i = 0; i < m_count; i++)
		values[i] = B.values[i];

	memcpy(indexes, B.indexes, m_count * sizeof(int));
	
	return *this;
}

template<typename ItemType, int Size>
ItemType SparcedVector<ItemType, Size>::getElement(const int index) const
{
	for (int i = 0; (i <= index) && (i < m_count); i++)
		if (indexes[i] == index)
			return values[i];
	return (ItemType) 0;
}

template<typename ItemType, int Size>
void SparcedVector<ItemType, Size>::setElement(const int index, const ItemType& value)
{
	int i, j;
	for (i = 0; (i <= index) && (i < m_count); i++)
		if (indexes[i] == index)
		{
			if (value != (ItemType) 0)
			{
				values[i] = value;		//обновляем значение элемента
				return;
			}
			else
			{							//удаляем ставший нулевым элемент
				m_count--;
				for (j = i; j < m_count; j++)
				{
					values[j] = values[j + 1];
					indexes[j] = indexes[j + 1];
				}
				if (m_count < (m_buf_size / 4))
					reduce();
				return;
			}
		}
	
	//вставлем новый ненулевой элемент
	if (m_count == m_buf_size)		
		extend();

	for (j = m_count; j > i; j--)
	{
		values[j] = values[j - 1];
		indexes[j] = indexes[j - 1];
	}
	values[i] = value;
	indexes[i] = index;
	m_count++;
}

template<typename ItemType, int Size>
SparcedVector<ItemType, Size> SparcedVector<ItemType, Size>::operator+ (const BaseVector<ItemType, Size>& B) const
{
	SparcedVector<ItemType, Size> toReturn(*this);
	return (toReturn += B);
}

template<typename ItemType, int Size>
SparcedVector<ItemType, Size> SparcedVector<ItemType, Size>::operator- (const BaseVector<ItemType, Size>& B) const
{
	SparcedVector<ItemType, Size> toReturn(*this);
	return (toReturn -= B);
}

template<typename ItemType, int Size>
SparcedVector<ItemType, Size> SparcedVector<ItemType, Size>::operator- ()
{
	SparcedVector<ItemType, Size> toReturn(*this);
	for (int i = 0; i < toReturn.m_count; i++)
		toReturn.values[i] = -toReturn.values[i];
	return toReturn;
}

template<typename ItemType, int Size>
void SparcedVector<ItemType, Size>::extend()
{
	m_buf_size = 2 * m_buf_size;
	ItemType* new_values = new ItemType[m_buf_size];
	int* new_indexes = new int[m_buf_size];

	for (int i = 0; i < m_count; i++)
		new_values[i] = values[i];

	memcpy(new_indexes, indexes, m_count * sizeof(int));

	delete[] values;
	delete[] indexes;

	values = new_values;
	indexes = new_indexes;
}

template<typename ItemType, int Size>
void SparcedVector<ItemType, Size>::reduce()
{
	m_buf_size = m_buf_size / 2;
	ItemType* new_values = new ItemType[m_buf_size];
	int* new_indexes = new int[m_buf_size];

	for (int i = 0; i < m_count; i++)
		new_values[i] = values[i];

	memcpy(new_indexes, indexes, m_count * sizeof(int));

	delete[] values;
	delete[] indexes;

	values = new_values;
	indexes = new_indexes;
}

template <typename ItemType, int Size>
SparcedVector<ItemType, Size>::~SparcedVector()
{
	delete[] values;
	delete[] indexes;
}


