#pragma once
#include "BaseMatrix.h"
#include "SimpleVector.h"
#include "SparcedVector.h"

template <typename ItemType, int Size, 
			template <typename T1, int rSize> class rows,
			template <typename T2, int cSize> class columns>
class Matrix :
	public BaseMatrix <ItemType, Size>
{
private:
	rows<columns<ItemType,Size>,Size> matrix;

public:
	Matrix () {}
	Matrix(const ItemType* A[]);
	Matrix(const BaseMatrix<ItemType, Size>& exist);
	Matrix(const Matrix<ItemType, Size, rows, columns>& exist);

	virtual ItemType getElement(const int rowIndex, const int columnIndex) const;
	virtual void setElement(const int rowIndex, const int columnIndex, const ItemType& value);

	virtual Matrix<ItemType, Size, rows, columns>& operator= (const BaseMatrix<ItemType, Size>& B);
	Matrix<ItemType, Size, rows, columns>& operator= (const Matrix<ItemType, Size, rows, columns>& B);
	Matrix<ItemType, Size, rows, columns> operator* (const BaseMatrix<ItemType, Size>& B) const;
	Matrix<ItemType, Size, rows, columns> operator+ (const BaseMatrix<ItemType, Size>& B) const;
	Matrix<ItemType, Size, rows, columns> operator- (const BaseMatrix<ItemType, Size>& B) const;
	Matrix<ItemType, Size, rows, columns> operator- () const;

	columns<ItemType, Size> operator* (const BaseVector<ItemType, Size>& B) const;

	virtual ~Matrix() {}
};

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns>::Matrix(const ItemType* A[])
{
	int i, j;

	//реализация "в лоб"
	/*
	for (i = 0; i < Size; i++)
	for (j = 0; j < Size; j++)
	setElement(i, j, A[i][j]);
	*/

	//оптимизированная реализация
	//записываем не элементы по отдельности,
	//а формируем строки и записываем их целиком
	for (i = 0; i < Size; i++)
	{
		columns<ItemType, Size> row;
		for (j = 0; j < Size; j++)
			row.setElement(j, A[i][j]);
		matrix.setElement(i, row);
	}
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns>::Matrix(const BaseMatrix<ItemType, Size>& exist)
{
	int i, j;

	//реализация "в лоб"
	/*
	for (i = 0; i < Size; i++)
	for (j = 0; j < Size; j++)
	setElement(i, j, exist.getElement(i,j));
	*/

	//оптимизированная реализация
	//записываем не элементы по отдельности,
	//а формируем строки и записываем их целиком
	for (i = 0; i < Size; i++)
	{
		columns<ItemType, Size> row;
		for (j = 0; j < Size; j++)
			row.setElement(j, exist.getElement(i, j));
		matrix.setElement(i, row);
	}
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns>::Matrix(const Matrix<ItemType, Size, rows, columns>& exist)
{
	//копируем построчно
	for (int i = 0; i < Size; i++)
		matrix.setElement(i, exist.matrix.getElement(i));
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
ItemType Matrix<ItemType, Size, rows, columns>::getElement(const int rowIndex, const int columnIndex) const
{
	return matrix.getElement(rowIndex).getElement(columnIndex);
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
void Matrix<ItemType, Size, rows, columns>::setElement(const int rowIndex, const int columnIndex, const ItemType& value)
{
	columns<ItemType, Size> row;
	row = matrix.getElement(rowIndex);
	row.setElement(columnIndex, value);
	matrix.setElement(rowIndex, row);
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns>& Matrix<ItemType, Size, rows, columns>::operator= (const Matrix<ItemType, Size, rows, columns>& B)
{
	//копируем построчно
	for (int i = 0; i < Size; i++)
		matrix.setElement(i, B.matrix.getElement(i));
	return *this;
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns>& Matrix<ItemType, Size, rows, columns>::operator= (const BaseMatrix<ItemType, Size>& B)
{
	int i, j;

	//копируем построчно
	for (i = 0; i < Size; i++)
	{
		columns<ItemType, Size> row;
		for (j = 0; j < Size; j++)
			row.setElement(j, B.getElement(i, j));
		matrix.setElement(i, row);
	}
	return *this;
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns> Matrix<ItemType, Size, rows, columns>::operator* (const BaseMatrix<ItemType, Size>& B) const
{
	int i, j;
	Matrix<ItemType, Size, rows, columns> B_transposed(B);
	B_transposed.transpose();
	Matrix<ItemType, Size, rows, columns> toReturn;
	
	for (i = 0; i < Size; i++)
	{
		columns<ItemType, Size> row = matrix.getElement(i);
		for (j = 0; j < Size; j++)
		{
			columns<ItemType, Size> column = B_transposed.matrix.getElement(j);
			toReturn.setElement(i, j, row*column);
		}
	}
	return toReturn;
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns> Matrix<ItemType, Size, rows, columns>::operator+ (const BaseMatrix<ItemType, Size>& B) const
{
	int i, j;
	Matrix<ItemType, Size, rows, columns> toReturn;

	//работаем построчно
	for (i = 0; i < Size; i++)
	{
		columns<ItemType, Size> row;
		for (j = 0; j < Size; j++)
			row.setElement(j, getElement(i, j) + B.getElement(i, j));
		toReturn.matrix.setElement(i, row);
	}
	return toReturn;
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns> Matrix<ItemType, Size, rows, columns>::operator- (const BaseMatrix<ItemType, Size>& B) const
{
	int i, j;
	Matrix<ItemType, Size, rows, columns> toReturn;

	//работаем построчно
	for (i = 0; i < Size; i++)
	{
		columns<ItemType, Size> row;
		for (j = 0; j < Size; j++)
			row.setElement(j, getElement(i, j) - B.getElement(i, j));
		toReturn.matrix.setElement(i, row);
	}
	return toReturn;
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
Matrix<ItemType, Size, rows, columns> Matrix<ItemType, Size, rows, columns>::operator- () const
{
	int i, j;
	Matrix<ItemType, Size, rows, columns> toReturn;

	//работаем построчно
	for (i = 0; i < Size; i++)
	{
		columns<ItemType, Size> row;
		for (j = 0; j < Size; j++)
			row.setElement(j, -getElement(i, j));
		toReturn.matrix.setElement(i, row);
	}
	return toReturn;
}

template <typename ItemType, int Size,
	template <typename T1, int rSize> class rows,
	template <typename T2, int cSize> class columns>
columns<ItemType, Size> Matrix<ItemType, Size, rows, columns>::operator* (const BaseVector<ItemType, Size>& B) const
{
	columns<ItemType, Size> toReturn;

	for (int i = 0; i < Size; i++)
		toReturn.setElement(i, matrix.getElement(i)*B);

	return toReturn;
}
